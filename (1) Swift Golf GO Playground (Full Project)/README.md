# Code Sample (1)

- Project Name: Golf GO
- Language: Swift
- Coverage: Game Structure (ECS), Game Data (Procedrual Generation), Game Logic (Golf & Camera), System API Calls (iOS)

# Introduction

This directory contains the full source codes (buildable) of my Apple WWDC 2018 scholarship winner project, Golf GO, which is a golf game capable of generating thousands of different golf course maps within 1000 lines of Swift code.

Entity-component-system (ECS) is a common architectural pattern used in game development. Apple provides such abstract layers in GamePlayKit with GKComponent and GKEntity, but without a proper bridge to SceneGraph objects like SCNNode in SceneKit. In Golf GO, I wrote a wrapper called JZGameObject that contains both references to the GamePlayKit and SceneKit and works in a way similar to Unity's MonoBehavior. For example, a GKComponent subclass called JZCameraFollowBehavior would have the render delegate of SceneKit dispatched to its update methods every frame from the mounting JZGameObject, in which it can access and modify the Transform property to make camera entity to follow the moving golf ball.

In Golf GO, golf course heightmaps are generated from noise generators like GKBillowNoiseSource from Apple GamePlayKit. Then the heightmap as a 2D array would be applied to a plane mesh, raising the z-axis of each vertex on the plane by the value in the corresponding 2D array. After the offset modification, a MDLMesh from ModelIO framework would be used to generate normals based on the new terrain, giving it an accurate appearance when combined with a lighting model.

# Contents

- Content.swift (999 LOC)