# Code Sample (2)

- Project Name: Shader Node ObjC
- Language: Objective-C
- Coverage: Data Abstraction, OOP, Algorithm

# Introduction

This directory contains the source codes of my Shader Node Editor project. Due to the 1000 lines limits, this zip only includes parts of the data model and thus cannot be successfully compiled. For complete projects using MVC pattern, please visit [NodeEditorObjC](https://github.com/JustinFincher/NodeEditorObjC) for Objective-C version or [WWDC19Playground](https://github.com/JustinFincher/WWDC19Playground) for Swift version.

Basically, the data model is responsible for generating shader codes for each node based on its knowledge on the node graph. For that goal, I deployed a two-pass approach in the NodeGraphData class. In the 1st pass, Shader Node would search the graph using DFS, collect linkage information, and build up a trace path for each node. In the 2nd pass, Shader Node would declare variables for each knot on nodes and equal operations on linked knots, and finally, append the code in order of inversed trace path.

# Contents

- NodeConnectionData.h (20 LOC)
- NodeConnectionData.m (22 LOC)
- NodeData.h (65 LOC)
- NodeData.m (182 LOC)
- NodeGraphData.h (43 LOC)
- NodeGraphData.m (210 LOC)
- NodePortData.h (41 LOC)
- NodePortData.m (91 LOC)
- NodePorts
	- NumberFloatNodePortData.h (18 LOC)
	- NumberFloatNodePortData.m (33 LOC)
- Nodes
	- AddNodeData.h (17 LOC)
	- AddNodeData.m (80 LOC)